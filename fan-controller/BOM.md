# FAN controller BOM

- Plastic enclosure
- 2 foot switches
    - switching power
    - switching speed
- fuse
- fuse holder
- power switch, 230V, DPST
- PCB transformer 230/12V
- rectifier bridge, small 40Vrms
- LM7812 linear voltage regulator

